package ;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.group.FlxSpriteGroup;

/**
 * ...
 * @author 
 */
class Skdimarks extends FlxSpriteGroup
{

	private var _speedY : Float;
	public function new() 
	{
		_speedY = 0;
		super();
	}
	
	public function SetSpeed(sy:Float)
    {
        _speedY = sy;
    }
	
	public override function update () : Void 
	{
		super.update();
		forEach(function(s:FlxSprite) : Void 
		{
			s.y +=  _speedY * FlxG.elapsed;
			if (s.y >  FlxG.height + s.height)
			{
				s.alive = false;
			}
		});
	}
	
}
package ;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.util.FlxPoint;

/**
 * ...
 * @author 
 */
class Tile extends FlxSprite
{

	public var blocked : Bool = false;
	public var blockDamageFactor : Float;
    private var _speedY : Float;
	public var _destroyable : Bool;
	
	public function new(t: TileType, p: FlxPoint) 
	{
		super();
		
        _destroyable = false;
		// loading sprite and rescaling
		if (t == TileType.Road)
		{
			loadGraphic(AssetPaths.road__png, false, 32, 32);
			blockDamageFactor = 0;
		}
		else if (t == TileType.Grass)
		{
			
			loadGraphic(AssetPaths.grass__png, false, 32, 32);
			blocked = true;
			blockDamageFactor = 0.25;
		}
		else if (t == TileType.Tree)
		{
			loadGraphic(AssetPaths.tree__png, false, 32, 32);
			blocked = true;
			blockDamageFactor = 10;
			_destroyable = false;
		}
		else if (t == TileType.Rock)
		{
			loadGraphic(AssetPaths.rock__png, false, 32, 32);
			blocked = true;
			blockDamageFactor = 2.0;
			_destroyable = true;
		}
		this.scale.set(2, 2);
		updateHitbox();
		
		// Set Position
		x = p.x;
		y = p.y;
	}
    
    public function SetSpeed(sy:Float)
    {
        _speedY = sy;
    }
    
	
	public override function update () : Void 
	{
		super.update();
		
		if (this.y >  FlxG.height + this.height)
		{
			alive = false;
		}
		
		this.y +=  _speedY * FlxG.elapsed;
	}
	
}
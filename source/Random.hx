package ;
import flixel.util.FlxRandom;

/**
 * ...
 * @author 
 */
class Random
{
	
	static public function floatRangeExclude ( min: Float, max : Float, exMin : Float, exMax : Float) : Float
	{
		var r : Float  = FlxRandom.floatRanged(min, max);
		if (r > exMin && r < exMax)
			return Random.floatRangeExclude(min, max, exMin, exMax);
		else
			return r;
	}
	
}
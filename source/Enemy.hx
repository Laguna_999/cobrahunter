package ;
import flixel.FlxG;
import flixel.system.FlxSound;
import flixel.tweens.FlxTween;
import flixel.util.FlxRandom;
import flixel.util.FlxTimer;

/**
 * ...
 * @author 
 */
class Enemy extends Car
{
	private var _slower : Bool;
	private var _aio :  AIOverseer;
	private var _aiTimer : Float = 0;
	
	private var _AItx : Float = 0;
	private var _AIty : Float = 0;
	
	public var _playerHasTouched : Bool;
    private var _playerHasTouchedTimer : Float;
	
	// enemy will drive crazy if hetakes damage
	private var _takeDamageTimer : Float;
	private var _takeDamageDir : Bool;
	
	private var _ending : Bool;
	

	
	public function new( s : Bool) 
	{
		super();
		// loading sprite and rescaling
		this.loadGraphic(AssetPaths.enemycar__png, true, 18, 32);
        this.animation.add('basic', [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15], 12);
        this.animation.play('basic');
		this.scale.set(1.5, 1.5);
		updateHitbox();
		
		_ending = false;
		
		// health stuff
		this.health = GameProperties.Enemy_DefaultHealth;
		_slower = s;
		
		_aiTimer = 0;
		
		_speedFactor = 0.75;
		_playerHasTouched  = false;
        _playerHasTouchedTimer = 1.0;
		
		_takeDamageTimer = 0;
		
		

	}

	
	public override function TakeDamage(d:Float) : Void 
	{
		
		if (_takeDamageTimer <= 0)
		{
			_takeDamageDir = FlxRandom.chanceRoll();
		}
		_takeDamageTimer = 0.5;
		super.TakeDamage(d);
		
	}
	
	
	override function CheckDead():Void 
	{
		if (this.health <= 0)
		{
			if ( !_ending)
			{
				_ending = true;
				// start displaying smoke or something like that 
				FlxTween.tween(this, { angle : (FlxRandom.chanceRoll()? 90 : -90) }, 0.6);
				_sfxExplode.play();
				var t : FlxTimer = new FlxTimer(4, function  ( t : FlxTimer) { alive = false; } );
			}
		}
	}
	
	
	public function setAIInfo(aio :AIOverseer) : Void 
	{
		_aio = aio;
	}
	
	public override function update() : Void 
	{
		super.update();
		if (!_ending)
		{
			
			DoAIStuff();
			doMovement();
			
			if (_playerHasTouched)
			{
				_playerHasTouchedTimer -= FlxG.elapsed;
				if ( _playerHasTouchedTimer <= 0)
				{
					_playerHasTouched = false;
				}
			}
			
		}
		else
		{
			this.y += GameProperties.Tile_CalcMoveSpeed() * FlxG.elapsed;
		}
		CheckOnScreen();
	}
	
    public function TouchEnemy ( ) : Void 
    {
        _playerHasTouchedTimer = 0.75;
        _playerHasTouched = true;
    }
	
	
	private function DoAIStuff () : Void 
	{
		_aiTimer -= FlxG.elapsed;
		if (_aiTimer <= 0)
		{
			_AItx = FlxRandom.floatRanged( _aio._center - _aio._width + GameProperties.Tile_Height ,  _aio._center + _aio._width - GameProperties.Tile_Height );
			_AIty  = y + FlxRandom.floatRanged(150, 400) * ((_slower) ? 1.0 : -1.0);
			
			_aiTimer += GameProperties.Tile_NewRoadStuffTimer;
		}
		
		if (_takeDamageTimer <= 0)
		{
			if (Math.abs((_AItx - x)) > GameProperties.Tile_Height/2)
			{
				if ((_AItx - x) > 0  )
				{
					steerRight();
				}
				else 
				{
					steerLeft();
				}
			}
			
			if (Math.abs((_AIty - y)) > GameProperties.Tile_Height/2)
			{
				if ((_AIty - y) > 0)
				{
					steerDown();
				}
				else 
				{
					steerUp();
				}
			}
		}
		else
		{
			_takeDamageTimer -= FlxG.elapsed;
			//if (_takeDamageDir)
			//{
				//steerLeft();
				//steerDown();
			//}
			//else 
			//{
				//steerRight();
				//steerDown();
			//}
		}
	}
	
	function CheckOnScreen():Void 
	{
		if (_slower)
		{
			if (this.y >  FlxG.height + this.height)
			{
				alive = false;
			}
		}
		else
		{
			if (this.y <  0 - this.height)
			{
				alive = false;
			}
		}
		

	}
	
	
	
	
	
}
package ;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.system.FlxSound;

/**
 * ...
 * @author 
 */
class Car extends FlxSprite
{

	
	private var _damageTimer : Float;
	
	private var _speedFactor : Float;
	
	private var _skids : Skdimarks;
	private var _skidTimer : Float;
	
	private var _sfxExplode : FlxSound;
	
	public function new() 
	{
		super();
		_damageTimer = 0.0;
		_speedFactor = 1.0;
		_skids = new Skdimarks();
		_skidTimer  = 0;
		
		_sfxExplode = new FlxSound();
		_sfxExplode = FlxG.sound.load(AssetPaths.explo__mp3, 0.75, false, false, false);
	}
	
	
	public override function draw(): Void 
	{
		super.draw();
	}
	
	

	
	
	
	
	
	public override function update() : Void 
	{
		super.update();
		
		_skidTimer  -= FlxG.elapsed;
		updateSkids();
		
		if (_damageTimer >= 0)
		{
			_damageTimer -= FlxG.elapsed;
		}
	}
	
	function doMovement():Void 
	{
		var vx : Float = velocity.x;
		var vy : Float = velocity.y;
		
		var mssx = GameProperties.Car_MaxSteeringSpeed.x * GameProperties.Tile_CalcMoveSpeed() ;
		var mssy = GameProperties.Car_MaxSteeringSpeed.y; // * GameProperties.Tile_CalcMoveSpeed();
		
		if (vx > mssx)
			vx = mssx;
		if (vx < -mssx)
			vx = -mssx;
			
		if (vy > mssy  * GameProperties.Car_BrakeSpeedFactor)
			vy = mssy * GameProperties.Car_BrakeSpeedFactor;
		if (vy < -mssy)
			vy = -mssy;
			
		velocity = new flixel.util.FlxPoint(vx * GameProperties.Car_SteeringVelocityDecay * _speedFactor, vy * GameProperties.Car_SteeringVelocityDecay * _speedFactor);
		
		// rotation
		var f : Float = vx / (GameProperties.Car_MaxSteeringSpeed.x * GameProperties.Tile_MoveSpeedFactor) ;
		
		if (Math.abs(f) > 0.25)
			AddSkids();
		
		this.angle = f * 8;
		
	}
	
	
	public function steerLeft() : Void 
	{
		velocity.x -= GameProperties.Car_SteeringSpeed;
	}
	
	public function steerRight() : Void 
	{
		velocity.x += GameProperties.Car_SteeringSpeed;
	}
	
	private function steerUp() : Void 
	{
		velocity.y -= GameProperties.Car_SteeringSpeed;
	}
	
	private function steerDown() : Void 
	{
		velocity.y += GameProperties.Car_SteeringSpeed;
		AddSkids();
	}
	

	
	public function TakeDamage(d:Float) : Void 
	{
		if (_damageTimer <= 0)
		{
			_damageTimer += GameProperties.Damage_DamageTimerMax;
			this.health -= d;
		}
		
		CheckDead();
	}
	
	
	public function drawSkids() : Void 
	{
		_skids.draw();
	}
	
	public function AddSkids() : Void 
	{
		if (_skidTimer <= 0)
		{
			{
				var skid : FlxSprite = new FlxSprite();
				skid.loadGraphic(AssetPaths.skids__png, false, 3, 6);
				skid.scale.set(1.5, 1.5);
				skid.x = this.x + 8;
				skid.y = this.y + this.height *0.75;
				skid.alpha = 0.85;
				
				_skids.add(skid);
			}
			{
				var skid : FlxSprite = new FlxSprite();
				skid.loadGraphic(AssetPaths.skids__png, false, 3, 6);
				skid.scale.set(1.5, 1.5);
				skid.x = this.x + this.width * 0.75;
				skid.y = this.y + this.height *0.75;
				skid.alpha = 0.85;
				_skids.add(skid);
				
			}
			_skidTimer = 0.002;
		}
	}
	
	function updateSkids():Void 
	{
		var sy = GameProperties.Tile_CalcMoveSpeed();
		_skids.SetSpeed(sy);
		_skids.update();
		var newSkids : Skdimarks = new Skdimarks();
		_skids.forEach(function (s:FlxSprite)
		{
			if (s.alive) { newSkids.add(s); }
			else { s.destroy(); }
		});
		
		_skids = newSkids;
	}
	
	function DieNow() : Void 
	{
		alive = false;
		_sfxExplode.play();
	}
	
	function CheckDead():Void 
	{
		if (this.health <= 0)
		{
			
			DieNow();
		}
	}
	
	
}
package ;

import flixel.FlxCamera;
import flixel.FlxG;
import flixel.FlxSprite;
import flixel.system.FlxSound;
import flixel.tweens.FlxTween;
import flixel.util.FlxColor;
import flixel.util.FlxColorUtil;
import flixel.util.FlxPoint;
import flixel.util.FlxVector;




/**
 * ...
 * @author 
 */
class Player extends Car
{

	// uses Flxsprite.health
	private var _healthBar : FlxSprite;
	private var _healthBarFull : FlxSprite;
	
	private var _ammo : Int;	// 
	private var _ammoMax : Int;
	private var _ammoBar : FlxSprite;
	private var _ammoBarFull : FlxSprite;
	private var _shootTimer : Float;
	
	private var _sfxSpeedUp : FlxSound;
	private var _sfxSpeedDown : FlxSound;
	private var _sfxCurve: FlxSound;
	private var _sfxShoot: FlxSound;
	
	
	
	private var _state : PlayState;
	public function new(state:PlayState) 
	{
		super();
		_state = state;
		// loading sprite and rescaling
		this.loadGraphic(AssetPaths.playercar__png, true, 20, 32);
        this.animation.add('basic', [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15], 12);
		
		this.animation.play('basic');

		this.scale.set(1.5, 1.5);
		updateHitbox();
		
		// set starting position
		x = FlxG.width / 2;
		y = FlxG.height / 4.0 * 3.0;
		
		// health stuff
		this.health = 100;
		
		_healthBar = new FlxSprite();
		_healthBar.makeGraphic(200, 24, FlxColor.CRIMSON);
		_healthBar.origin.set(0, 0);
		_healthBar.setPosition(10, 10);
		
		_healthBarFull = new FlxSprite();
		_healthBarFull.makeGraphic(210, 30, FlxColor.SILVER);
		_healthBarFull.origin.set(0, 0);
		_healthBarFull.offset.set(5, 3);
		_healthBarFull.setPosition(10, 10);
		FlxTween.color(_healthBarFull, 2.5, FlxColor.AQUAMARINE , FlxColor.INDIGO, 1.0, 1.0, { type:FlxTween.PINGPONG } );
		
		// AmmoStuff
		_ammoMax = 28;
		_ammo = 14;
		
		_ammoBar = new  FlxSprite();
		_ammoBar.makeGraphic(200, 24, FlxColor.INDIGO);
		_ammoBar.origin.set(0, 0);
		_ammoBar.setPosition(10, 44);
		
		
		_ammoBarFull = new FlxSprite();
		_ammoBarFull.makeGraphic(210, 30, FlxColor.SILVER);
		_ammoBarFull.origin.set(0, 0);
		_ammoBarFull.offset.set(5, 3);
		_ammoBarFull.setPosition(10, 44);
		FlxTween.color(_ammoBarFull, 2.5, FlxColor.AQUAMARINE , FlxColor.CRIMSON, 1.0, 1.0, { type:FlxTween.PINGPONG } );
		
		_shootTimer = 0;
		
		_sfxSpeedUp = new  FlxSound();
		_sfxSpeedUp = FlxG.sound.load(AssetPaths.SpeedUp__mp3, 0.25, false, false , false);
		
		_sfxSpeedDown = new  FlxSound();
		_sfxSpeedDown = FlxG.sound.load(AssetPaths.SpeedDown__mp3, 0.25, false, false , false);
		
		_sfxCurve = new FlxSound();
		_sfxCurve = FlxG.sound.load(AssetPaths.Curve__mp3, 0.025, false, false , false);
		
		_sfxShoot = new FlxSound();
		_sfxShoot = FlxG.sound.load(AssetPaths.Shoot__mp3, 0.5, false, false, false);
		
	}
	
	public override function draw() : Void
	{
		super.draw();
		
	}
	
	public function drawHud() : Void 
	{
		_healthBarFull.draw();
		_healthBar.draw();
		
		_ammoBarFull.draw();
		_ammoBar.draw();
	}
	
	
	override public function TakeDamage(d:Float):Void 
	{
		FlxG.camera.shake(0.01, GameProperties.Damage_DamageTimerMax / 2.0, function():Void { }, true, FlxCamera.SHAKE_HORIZONTAL_ONLY);
		var c: Int = FlxColorUtil.makeFromARGB(0.25, 200, 20, 20);
		FlxG.camera.flash(c, GameProperties.Damage_DamageTimerMax / 2.0);
		super.TakeDamage(d);
	}
	
	
	public override function update() : Void 
	{
		super.update();
		getInput();
		
		doMovement();
		
		CheckOnScreen();
		
		var f : Float = health / 100.0;
		if ( f < 0 ) f = 0;
		if (f > 100) f = 1;
		_healthBar.scale.set(f, 1);
		_healthBar.update();
		_healthBarFull.update();
		
		var a : Float = _ammo / _ammoMax;
		if (a < 0) a = 0;
		if (a > 1) a = 1;
		_ammoBar.scale.set(a, 1);
		_ammoBar.update();
		_ammoBarFull.update();
		if (_shootTimer >= 0)
			_shootTimer -= FlxG.elapsed;
	}
	
	
	private function getInput () : Void 
	{
		var left : Bool = false;
		var right : Bool = false;
		
		if (FlxG.keys.anyPressed(["A", "LEFT"]))
			left = true;
		if (FlxG.keys.anyPressed(["D", "RIGHT"]))
			right = true;
			
		if (left && !right)
		{
			steerLeft();
			_sfxCurve.play(false);
		}
		else if (right != left)
		{
			steerRight();
			_sfxCurve.play(false);
		}
		else
		{
			// do nothing, since both or no key is pressed
		}
		
		
		var up : Bool = false;
		var down : Bool = false;
		
		if (FlxG.keys.anyPressed(["W", "UP"]))
			up = true;
		if (FlxG.keys.anyPressed(["S", "DOWN"]))
			down = true;
			
		if (up && !down)
		{
			steerUp();
			GameProperties.Car_SpeedUp();
			_sfxSpeedDown.stop();
			_sfxSpeedUp.play(false);
		}
		else if (down != up)
		{
			steerDown();
			GameProperties.Car_SpeedDown();
			_sfxSpeedUp.stop();
			_sfxSpeedDown.play(false);
		}
		else
		{
			_sfxSpeedDown.stop();
			_sfxSpeedUp.stop();
			// do nothing, since both or no key is pressed
		}
		
		var shoot: Bool = false;
		if (FlxG.keys.anyPressed(["SPACE"]))
		{
			shoot = true;
		}
		if (shoot)
		{
			Shoot();
		}
	}
	
	function CheckOnScreen():Void 
	{
		var  takeDamage  :Bool = false;
		if (x < 0 || x - this.width > FlxG.width)
		{
			takeDamage = true;
		}
		if (y < 0 || y - this.height > FlxG.height)
		{
			takeDamage = true;
		}
		
		if (takeDamage)
		{
			trace ("outside");
			TakeDamage(GameProperties.Damage_CollisionDamage);
		}
	}
    
    
    public function pickUp ( pu : PickUp): Void 
    {
        if (pu._type == PickUpType.Repair)
        {
            health = 100;
        }
        else if (pu._type == PickUpType.MachineGun)
        {
            _ammo += Std.int(_ammoMax / 2);
			if (_ammo > _ammoMax)
				_ammo = _ammoMax;
        }
    }
	
	var _shotPos : Bool;
	
	private function Shoot() : Void
	{
		if (_shootTimer <= 0)
		{
			if ( _ammo > 0)
			{
				_shootTimer = GameProperties.Shot_TimerMax;
				var s: Shot = new  Shot (this.x + 10 +(_shotPos ? 0 : this.width*0.5) , this.y);
				_state.SpawnShot(s);
				_shotPos = !_shotPos;
				_ammo -= 1;
				//_ammoBar.color = FlxColor.SILVER;
				FlxTween.color(_ammoBar, GameProperties.Shot_TimerMax/2, FlxColor.SILVER, FlxColor.INDIGO);
				_sfxShoot.play(true);
			}
		}
	}
	
}
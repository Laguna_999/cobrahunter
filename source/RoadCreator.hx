package ;
import flixel.FlxG;
import flixel.group.FlxTypedGroup;
import flixel.util.FlxPoint;
import flixel.util.FlxRandom;

/**
 * ...
 * @author Laguna
 */
class RoadCreator
{
    public static function Create(_center:Int, _width:Int, topPos: Float, _tiles : FlxTypedGroup<Tile>, _tilesBlocked: FlxTypedGroup<Tile>)
    {
        var c : Bool = FlxRandom.chanceRoll(50);
        //if (_center <= 3 && c)
        //{
            //
        //}
        //else if (_center >= 11 && c)
        //{
            //
        //}
        //else
        //{
            CreateDefaultLine(_center, _width, topPos, _tiles, _tilesBlocked);
        //}
		
		var assi :Bool = FlxRandom.chanceRoll(0.5);
		
		if (assi)
		{
			CreateRoadBlock(_center, _width, topPos, _tiles, _tilesBlocked);
		}
    }
    
    
    
    static private function CreateDefaultLine(_center:Int, _width:Int, topPos: Float, _tiles : FlxTypedGroup<Tile>, _tilesBlocked: FlxTypedGroup<Tile>):Void 
    {
        var roadLeftBorder : Int = _center - _width +1;
        var roadRightBorder : Int = _center + _width;
        
        //trace ("create new line");
        for (i in -1...roadLeftBorder)
        {
            var t :Tile  = new Tile(TileType.Grass, new FlxPoint(i * GameProperties.Tile_Height, topPos - GameProperties.Tile_Height));
            _tilesBlocked.add(t);
        }
        for (i in roadLeftBorder...roadRightBorder)
        {
            var t : Tile = new Tile(TileType.Road, new FlxPoint(i * GameProperties.Tile_Height, topPos -GameProperties.Tile_Height ));
            _tiles.add(t);
        }
        for (i in roadRightBorder...14)
        {
            var t :Tile = new Tile(TileType.Grass, new FlxPoint(i * GameProperties.Tile_Height, topPos -GameProperties.Tile_Height ));
            _tilesBlocked.add(t);
        }
        

        while (FlxRandom.chanceRoll(50))
        {
            var px : Float = Random.floatRangeExclude( -32, FlxG.width + 32, (roadLeftBorder -1) * 64, roadRightBorder * 64);
            var t : Tile = new Tile(TileType.Tree, new FlxPoint( px , topPos -GameProperties.Tile_Height));
            _tilesBlocked.add(t);
        }
		
		if (FlxRandom.chanceRoll(GameProperties.Tile_RockSpawnChance))
		{
			var px : Float = FlxRandom.floatRanged(roadLeftBorder * GameProperties.Tile_Height, roadRightBorder * GameProperties.Tile_Height);
            var t : Tile = new Tile(TileType.Rock, new FlxPoint( px , topPos -GameProperties.Tile_Height));
            _tilesBlocked.add(t);
		}
    }
	
	
	static private function CreateRoadBlock(_center:Int, _width:Int, topPos: Float, _tiles : FlxTypedGroup<Tile>, _tilesBlocked: FlxTypedGroup<Tile>):Void 
    {
        var roadLeftBorder : Int = _center - _width +1;
        var roadRightBorder : Int = _center + _width;
        
        //trace ("create new line");
        for (i in -1...roadLeftBorder)
        {
            var t :Tile  = new Tile(TileType.Grass, new FlxPoint(i * GameProperties.Tile_Height, topPos - GameProperties.Tile_Height));
            _tilesBlocked.add(t);
        }
        for (i in roadLeftBorder...roadRightBorder)
        {
            var t : Tile = new Tile(TileType.Road, new FlxPoint(i * GameProperties.Tile_Height, topPos -GameProperties.Tile_Height ));
            _tiles.add(t);
			var r : Bool = FlxRandom.chanceRoll(50);
			
			if (i % 3 == (r? 0 : 1))
			{
				var t2 : Tile = new Tile(TileType.Rock, new FlxPoint(i * GameProperties.Tile_Height, topPos -GameProperties.Tile_Height ));
				_tilesBlocked.add(t2);
			}
        }
        for (i in roadRightBorder...14)
        {
            var t :Tile = new Tile(TileType.Grass, new FlxPoint(i * GameProperties.Tile_Height, topPos -GameProperties.Tile_Height ));
            _tilesBlocked.add(t);
        }
        
    }
}
package ;
import flixel.util.FlxPoint;

/**
 * ...
 * @author 
 */
class GameProperties
{
		public static var Car_SteeringSpeed = 100 ;
		public static var Car_MaxSteeringSpeed : FlxPoint = new FlxPoint(275, 155);
		public static var Car_BrakeSpeedFactor : Float = 1.45;
		public static var Car_SteeringVelocityDecay = 0.875;
		// will be controlled from outside this class // needs to be reset at gamestart
		public static var Car_PlayerSpeedFactor : Float = 1.0;
		public static var Car_PlayerSpeedFactorMax : Float = 1.25;
		public static function Car_SpeedUp() : Void 
		{
			GameProperties.Car_PlayerSpeedFactor *= 1.05;
			if (GameProperties.Car_PlayerSpeedFactor >= GameProperties.Car_PlayerSpeedFactorMax)
			{
				GameProperties.Car_PlayerSpeedFactor = GameProperties.Car_PlayerSpeedFactorMax;
			}
		}
		public static function Car_SpeedDown() : Void 
		{
			GameProperties.Car_PlayerSpeedFactor *= 0.95;
			if (GameProperties.Car_PlayerSpeedFactor <= 1.0)
			{
				GameProperties.Car_PlayerSpeedFactor = 1.0;
			}
		}
		
		public static var Damage_CollisionDamage = 15;
		public static var Damage_DamageTimerMax = 0.125;
		
		public static var Tile_Height : Float = 64;
		public static var Tile_MoveSpeed : Float = 295 ;
		public static var Tile_NewRoadStuffTimer : Float = 2.0;
		public static var Tile_MoveSpeedFactorIncrease : Float = 0.1;
		public static var Tile_SpeedUpTimer : Float = 5;
		// will be controlled from outside this class // needs to be reset at gamestart
		public static var Tile_MoveSpeedFactor : Float = 1.0;
		public static var Tile_RockSpawnChance : Float = 3;
		
		
		public static var Score_BasicIncome : Float = 1.0;
		public static var Score_EnemyFixedRate : Float = 75;
        public static var Score_PickUpIncome : Float = 50;
		
		public static var Enemy_DefaultHealth : Float = 30;
		public static var Enemy_SpawnTimerMax : Float = 3.5;

		public static var Shot_TimerMax : Float = 0.075;
		public static var Shot_Damage : Float = 12.5;
		
		public static function Tile_CalcMoveSpeed () : Float 
		{
			var fac : Float = GameProperties.Tile_MoveSpeedFactor;
			
			if (fac > 1.75 && fac < 3.5)
			{
				fac = 1.75;
			}
			if (fac >= 3.5)
			{
				fac -= 1.75;
			}
			if ( fac > 5.5)
			{	
				fac = 5.5;
			}
			var f : Float = GameProperties.Tile_MoveSpeed * GameProperties.Car_PlayerSpeedFactor * Math.pow(fac, 1.75);
		
			return f;
		}
		
}
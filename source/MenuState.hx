package;

import flixel.addons.api.FlxGameJolt;
import flixel.addons.tile.FlxTileAnimation;
import flixel.FlxG;
import flixel.FlxSprite;
import flixel.FlxState;
import flixel.text.FlxText;
import flixel.tweens.FlxTween;
import flixel.tweens.misc.ColorTween;
import flixel.ui.FlxButton;
import flixel.util.FlxColor;
import flixel.util.FlxColorUtil;
import flixel.util.FlxMath;
import flash.utils.ByteArray;
import flixel.util.FlxTimer;

 using flixel.util.FlxSpriteUtil;

/**
 * These lines allow embedding of assets as ByteArrays, which helps to minimize the threat of data being compromised.
 * For your own purposes, it is recommended you add "*.privatekey" to your source control ignore list.
 * The content of the .privatekey file should be just your private key.
 * To see how this file is read and used, look at the bottom of the create() function below.
 */
@:file("assets/data/cobra.privatekey") class MyPrivateKey extends ByteArray { }
/**

/**
 * A FlxState which can be used for the game's menu.
 */
class MenuState extends FlxState
{
	
	private var _titleText : FlxText;
	private var _titleText2 : FlxText;

	private var _loginText : FlxText;
	private var _loginText2 : FlxText;
	
	private var _playText : FlxText;
	private var _playText2 : FlxText;
	
	private var _logo : FlxSprite;
	private var _logo2 : FlxSprite;
	

	
	/**
	 * Function that is called up when to state is created to set it up. 
	 */
	override public function create():Void
	{
		super.create();
		var ba:ByteArray = new MyPrivateKey();
		
		_titleText2 = new FlxText(0, 0, 500);
		_titleText2.text = "Cobra Hunter";
		_titleText2.setFormat("assets/data/AeroviasBrasilNF.ttf", 100, FlxColor.WHITE,"center");
		_titleText2.screenCenter();
		_titleText2.y -= FlxG.height / 2;
		_titleText2.offset.set( -4, 4);
		
		//FlxTween.tween(_titleText2.origin, { x: -8, y : 7 }, 1.25, { type:FlxTween.PINGPONG } );
		FlxTween.color(_titleText2, 2.5, FlxColor.CRIMSON, FlxColor.AQUAMARINE, 0.5, 0.5, {type:FlxTween.PINGPONG} );
		
		add(_titleText2);
		
		_titleText = new FlxText(0, 0, 500);
		_titleText.text = "Cobra Hunter";
		_titleText.setFormat("assets/data/AeroviasBrasilNF.ttf", 100, FlxColor.WHITE,"center");
		_titleText.screenCenter();
		_titleText.y -= FlxG.height / 2;
		add(_titleText);
		FlxTween.color(_titleText, 2.5, FlxColor.AQUAMARINE, FlxColor.CRIMSON, 1, 1, {type:FlxTween.PINGPONG} );
		
		
		// logo stuff
		_logo = new FlxSprite();
		_logo.loadGraphic(AssetPaths.logo__png, false, 852, 472);
		_logo.scale.set(0.25, 0.25);
		_logo.updateHitbox();
		_logo.x = 10;
		_logo.y = 600 - _logo.height - 10;
		add(_logo);
		
		_logo2 = new FlxSprite();
		_logo2.loadGraphic(AssetPaths.runvs__png, false, 1024, 1024);
		_logo2.scale.set(0.25, 0.25);
		_logo2.updateHitbox();
		_logo2.x = FlxG.width - _logo2.width - 10;
		_logo2.y = FlxG.height- _logo2.height + 70;
		add(_logo2);
		
		FlxG.sound.playMusic(AssetPaths.Cobra_Menu__mp3, 1.0, true);
		
		
		
		if (!FlxGameJolt.initialized) {
			FlxGameJolt.init(80048, ba.readUTFBytes(ba.length), true, null, null, initCallback);
		} else {
			_loginText2 = new FlxText(0, 0, 500);
			_loginText2.text = "Welcome back " + FlxGameJolt.username + "!";
			var c  =FlxColorUtil.makeFromARGB(0.5, FlxColorUtil.getRed(FlxColor.AQUAMARINE), FlxColorUtil.getGreen(FlxColor.AQUAMARINE), FlxColorUtil.getBlue(FlxColor.AQUAMARINE));
			_loginText2.setFormat("assets/data/AeroviasBrasilNF.ttf", 34, c ,"center");
			_loginText2.screenCenter();
			_loginText2.y -= FlxG.height / 4 - 100;
			_loginText2.offset.set( -4, 4);
			FlxTween.tween(_loginText2.origin, { x: -8, y : 7 }, 1.25, { type:FlxTween.PINGPONG } );
			FlxTween.color(_loginText2, 2.5, FlxColor.CRIMSON, FlxColor.AQUAMARINE, 0.5, 0.5, {type:FlxTween.PINGPONG} );
			
			add(_loginText2);
			
			_loginText = new FlxText(0, 0, 500);
			_loginText.text = "Welcome back " + FlxGameJolt.username + "!";
			_loginText.setFormat("assets/data/AeroviasBrasilNF.ttf", 34,  FlxColor.AQUAMARINE,"center");
			_loginText.screenCenter();
			_loginText.y -= FlxG.height / 4 - 100;
			FlxTween.color(_loginText, 2.5, FlxColor.AQUAMARINE, FlxColor.CRIMSON, 1, 1, { type:FlxTween.PINGPONG } );
			add(_loginText);
		}
		
		_playText2 = new FlxText(0, 0, 700);
		_playText2.text = "Press [Space] or [Return] to start";
		_playText2.setFormat("assets/data/AeroviasBrasilNF.ttf", 45, FlxColor.CRIMSON,"center");
		_playText2.screenCenter();
		_playText2.y += 120;
		_playText2.offset.set( -4, 4);
		
		add(_playText2);
		
		_playText = new FlxText(0, 0, 700);
		_playText.text = "Press [Space] or [Return] to start";
		_playText.setFormat("assets/data/AeroviasBrasilNF.ttf", 45,  FlxColor.AQUAMARINE,"center");
		_playText.screenCenter();
		_playText.y += 120;
		add(_playText);
		
		
		
		
	}
	
	private function initCallback(Result:Bool):Void
	{
		if (Result) 
		{
			//trace("Successfully connected to GameJolt! Hi " + FlxGameJolt.username + "!");
			
			//FlxGameJolt.addTrophy(5072);
			
			//if (_login.visible) {
				//_login.visible = false;
				//_login.active = false;
			//}
			//
			//if (_loginGroup.visible == true) {
				//switchMenu("Back");
			//}
			
			//FlxGameJolt.fetchAvatarImage( avatarCallback );
		}
		else
		{
			//trace ("Nope" );
		}
	}
	
	/**
	 * Function that is called when this state is destroyed - you might want to 
	 * consider setting all objects this state uses to null to help garbage collection.
	 */
	override public function destroy():Void
	{
		super.destroy();
	}

	/**
	 * Function that is called once every frame.
	 */
	override public function update():Void
	{
		super.update();
		if (FlxG.keys.anyJustPressed(["RETURN", "SPACE"]))
		{
			FlxG.camera.fade(FlxColor.BLACK, 0.6);
			FlxG.sound.music.fadeOut(0.6);
			
			var t : FlxTimer = new FlxTimer(0.65, function (t:FlxTimer) 
			{		
				FlxG.switchState(new PlayState());
			});
		}
	}	
}
package ;

import flixel.addons.tile.FlxTileAnimation;
import flixel.FlxG;
import flixel.FlxSprite;
import flixel.tweens.FlxTween;
import flixel.util.FlxRandom;
import flixel.util.FlxTimer;

/**
 * ...
 * @author Laguna
 */
class PickUp extends FlxSprite
{
    public var _type : PickUpType;
	public var ending : Bool;
	
	public static function getRandomPickupType (): PickUpType
    {
        var maxNumber = PickUpType.getConstructors().length -1;  
        
        var retval: PickUpType = PickUpType.createByIndex(FlxRandom.intRanged(0, maxNumber));

        return retval;
    }
	
	public var text : String;
    
    public function new( type : PickUpType) 
    {
        super();
		_type = type;
        
		ending = false;
		
		loadGraphic(AssetPaths.pickup__png, true, 16, 16);
		scale.set(2.0, 2.0);
		updateHitbox();
		animation.add("basic", [0, 1, 2, 3], 12);
		animation.play("basic");
		
        
		if (_type == PickUpType.MachineGun2)
		{
			_type = PickUpType.MachineGun;
		}
		
		if ( _type == PickUpType.MachineGun)
        {
            text = "Ammunition";
        }
        else if ( _type == PickUpType.Points)
        {
            text = "Score";
        }
        else if ( _type == PickUpType.Repair)
        {
            text = "Repair";
        }
		
    }
    
    public override function update () : Void 
	{
		super.update();
		
		if (this.y >  FlxG.height + this.height)
		{
			alive = false;
		}
		
		this.y += GameProperties.Tile_CalcMoveSpeed() * FlxG.elapsed * 0.75;
	}
    
	public function PickMeUp() : Void 
	{
		ending = true;
		FlxTween.tween(this.scale, { x:10, y:10 } );
		FlxTween.tween(this, { alpha:0 }, 0.9);
		var t: FlxTimer = new FlxTimer(1, function (t:FlxTimer) { this.alive = false; } );
	}
	
}
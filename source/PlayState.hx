package;

import flixel.addons.api.FlxGameJolt;
import flixel.FlxBasic;
import flixel.FlxG;
import flixel.FlxSprite;
import flixel.FlxState;
import flixel.group.FlxTypedGroup;
import flixel.system.FlxSound;
import flixel.text.FlxText;
import flixel.tweens.FlxTween;
import flixel.ui.FlxButton;
import flixel.util.FlxColor;
import flixel.util.FlxMath;
import flixel.util.FlxPoint;
import flixel.util.FlxRandom;
import flixel.util.FlxTimer;

/**
 * A FlxState which can be used for the actual gameplay.
 */
class PlayState extends FlxState
{
	
	private var _player : Player;
	
	private var _tiles: FlxTypedGroup<Tile>;
	private var _tilesBlocked: FlxTypedGroup<Tile>;
	
	private var _enemyCars : FlxTypedGroup<Enemy>;
    private var _pickUpList : FlxTypedGroup<PickUp>;
	
	private var _shotList : FlxTypedGroup<Shot>;
	
	private var _center : Int;
	private var _width : Int;
	private var _maxWidth : Int;
	private var _minWidth : Int;
	
	private var _scoreText : FlxText;
	private var _scoreText2 : FlxText;
	private var _score : Float;
    
    private var _infoText2 : FlxText;
    private var _infoText : FlxText;
    
	
	private var _speedUpTimer : FlxTimer;
	private var _scoreTimer : FlxTimer;
	
	private var _enemySpawnTimer : Float ;
    private var _pickUpSpawnTimer : Float;
	
	private var _vignette : FlxSprite;
    
    
	private var _sfxScore : FlxSound;
	private var _sfxRepair : FlxSound;
	private var _sfxAmmunition : FlxSound;
	
	private var _overlay : FlxSprite;
	
	
	private var _ending : Bool;
	
	/**
	 * Function that is called up when to state is created to set it up. 
	 */
	override public function create():Void
	{
		super.create();
		_player = new Player(this);
		GameProperties.Tile_MoveSpeedFactor = 1.0;
		GameProperties.Car_PlayerSpeedFactor = 1.0;
		GameProperties.Tile_RockSpawnChance = 3.0;
		
		#if flash
		FlxG.sound.playMusic(AssetPaths.CobraHunter__mp3, 1.0, true);
		#else
		FlxG.sound.playMusic(AssetPaths.CobraHunter__ogg, 1.0, true);
		#end
		
		_score = 0;
		_ending = false;
		
		_speedUpTimer = new FlxTimer(GameProperties.Tile_SpeedUpTimer, function(t:FlxTimer) : Void { speedUp(); }, 0);
		_scoreTimer = new FlxTimer(0.5, function(t:FlxTimer) : Void { _score += GameProperties.Score_BasicIncome  * GameProperties.Tile_MoveSpeedFactor * Math.pow(GameProperties.Tile_MoveSpeedFactor, 0.25); }, 0);
		var t : FlxTimer  = new FlxTimer(GameProperties.Tile_NewRoadStuffTimer, function(t:FlxTimer) : Void  { changeRoadParameters(); } , 1);
		
		initScore();
		initializeRoad();
		
		_enemyCars = new FlxTypedGroup<Enemy>();	
		_enemySpawnTimer = 100;
        
        _pickUpList  = new FlxTypedGroup<PickUp>();
        _pickUpSpawnTimer = 100;
        
        _vignette = new FlxSprite(); 
		_vignette.loadGraphic(AssetPaths.vignette__png, false, 800, 600);
		_vignette.alpha = 0.5;
		
		_overlay  = new FlxSprite();
		_overlay .makeGraphic(FlxG.width, FlxG.height, FlxColor.BLACK);
		FlxTween.tween(_overlay, { alpha:0 }, 0.5);
		
		_shotList = new FlxTypedGroup<Shot>();

        // Tutorial
		ShowInfoText(300, 300, "Use [wasd] or [arrows] to drive", 4);
        
        var t1: FlxTimer = new FlxTimer( 5, function (t:FlxTimer) { ShowInfoText(300, 300, "Pick Up those Powerups!", 4); } );
		var t11: FlxTimer = new FlxTimer( 5.75, function (t:FlxTimer) { SpawnPickUp(true); } );
        var t2: FlxTimer = new FlxTimer( 10, function (t:FlxTimer) { SpawnEnemy(); ShowInfoText(300, 300, "Golden Cobra gangers will try to ram you. Push them off the street!", 2.5); } );
		var t3: FlxTimer = new FlxTimer( 13, function (t:FlxTimer) { SpawnEnemy(); ShowInfoText(300, 300, "Shoot them with [space], but watch your ammo!", 4); } );
		var t4: FlxTimer = new FlxTimer( 17, function (t:FlxTimer) { ShowInfoText(300, 300, "Good luck, Agent!", 4); } );
		
		// Sound Stuff
		_sfxScore = new FlxSound();
		_sfxScore = FlxG.sound.load(AssetPaths.Score__mp3, 1.0, false, false , false);
				
		_sfxRepair = new FlxSound();
		_sfxRepair = FlxG.sound.load(AssetPaths.Repair__mp3, 1.0, false, false , false);
		
		_sfxAmmunition = new FlxSound();
		_sfxAmmunition = FlxG.sound.load(AssetPaths.Ammunition__mp3, 1.0, false, false , false);
	}
	
	private function cleanUp():Void
	{
		var newTiles : FlxTypedGroup<Tile> =  new FlxTypedGroup<Tile>();
		_tiles.forEach(function (t: Tile ) : Void 
		{
			if (t.alive) { newTiles.add(t); } else { t.destroy(); }
		});
		_tiles = newTiles;
		
		var newTilesBlocked : FlxTypedGroup<Tile> =  new FlxTypedGroup<Tile>();
		_tilesBlocked.forEach(function (t: Tile ) : Void 
		{
			if (t.alive) { newTilesBlocked.add(t); } else { t.destroy(); }
		});
		_tilesBlocked = newTilesBlocked;
		
		var newShots : FlxTypedGroup<Shot> =  new FlxTypedGroup<Shot>();
		_shotList.forEach(function (s: Shot ) : Void 
		{
			if (s.alive) { newShots.add(s); } else { s.destroy(); }
		});
		_shotList = newShots;
		
		
		var newEnemyList : FlxTypedGroup<Enemy> = new FlxTypedGroup<Enemy>();
		_enemyCars.forEach(function (e:Enemy) : Void 
		{
			if (e.alive) { newEnemyList.add(e); } else 
			{ 
				if (e._playerHasTouched )
				{
					var s : Float = GameProperties.Score_EnemyFixedRate * Math.pow(GameProperties.Tile_MoveSpeedFactor, 0.25);
					_score += s;
					ShowInfoText(e.x, e.y, Std.string(Std.int(s)));
				}
			}
		});
		_enemyCars = newEnemyList;
		
		var newPickUps : FlxTypedGroup<PickUp>  = new  FlxTypedGroup<PickUp>  ();
		_pickUpList.forEach(function (pu: PickUp) : Void 
		{
			if (pu.alive) { newPickUps.add(pu); } else { pu.destroy(); }
		});
		_pickUpList = newPickUps;
		
		
	}
	
	
	/**
	 * Function that is called when this state is destroyed - you might want to 
	 * consider setting all objects this state uses to null to help garbage collection.
	 */
	override public function destroy():Void
	{
		super.destroy();
	}

	/**
	 * Function that is called once every frame.
	 */
	override public function update():Void
	{
		super.update();
		
		if (!_ending)
		{
			_player.update();
            
            if (!_player.alive)
			{
				endGame();
			}
            
            cleanUp();
			_overlay.update();

            // Tiles
            var sy = GameProperties.Tile_CalcMoveSpeed();
            _tiles.forEachAlive(function (t:Tile) { t.SetSpeed(sy); } );
            _tilesBlocked.forEachAlive(function (t:Tile) { t.SetSpeed(sy); } );
			_tiles.update();
			_tilesBlocked.update();
			
            // Enemies
			var aio : AIOverseer = new AIOverseer(_center * GameProperties.Tile_Height, _width * GameProperties.Tile_Height);
			_enemyCars.forEachAlive(function (e:Enemy) : Void { e.setAIInfo(aio); } );
			_enemyCars.update();
            
			
            _enemySpawnTimer -= FlxG.elapsed;
			if (_enemySpawnTimer <= 0)
			{
				SpawnEnemy();
				_enemySpawnTimer += GameProperties.Enemy_SpawnTimerMax;
			}
            
			// Shots
			_shotList.update();
			FlxG.overlap(_shotList, _enemyCars, function(s:Shot, e:Enemy): Void 
			{
				if (FlxG.pixelPerfectOverlap(s, e, 0))
				{
					s.alive = false;
					e.TakeDamage(GameProperties.Shot_Damage);
					e.TouchEnemy();
				}
			});
			
			FlxG.overlap(_shotList, _tilesBlocked, function(s:Shot, t:Tile): Void 
			{
				if (t._destroyable)
				{
					s.alive = false;
					t.alive = false;
				}
			});
			
            
            // Pickups
            _pickUpList.update();
			_pickUpSpawnTimer -= FlxG.elapsed;
			if (_pickUpSpawnTimer <= 0)
			{
				SpawnPickUp();
			}
            
			// score
			_scoreText2.text = Std.string(Std.int(_score));
			_scoreText2.update();
			_scoreText.text = Std.string(Std.int(_score));
			_scoreText.update();
            
            // Info
            _infoText2.update();
            _infoText.update();
			
            
            // just to make sure
            FlxG.collide(_enemyCars, _enemyCars);
            
            // player enemy collision
			FlxG.collide(_player, _enemyCars, function (p:Player, e:Enemy) : Void 
			{
				p.AddSkids();
				e.AddSkids();
				//p.TakeDamage(GameProperties.Damage_CollisionDamage * 0.125);
				e.TakeDamage(GameProperties.Damage_CollisionDamage * 0.25);
				if ( e.x - p.x > 0)
					e.steerRight();
				else 
					e.steerLeft();
				e.TouchEnemy();
				trace ("collision");
			} );
            FlxG.overlap(_player, _pickUpList, overlapPlayerPickups);
			FlxG.overlap(_enemyCars, _tilesBlocked, overlapPlayerTiles);
			FlxG.overlap(_player, _tilesBlocked, overlapPlayerTiles);
			//FlxG.overlap(_enemyCars, _tilesBlocked, overlapEnemyTiles);

			PushNewLine();	
		}
	}	
	
	
	override public function draw() : Void 
	{
		_tiles.draw();
		_tilesBlocked.draw();
		
		_player.drawSkids();
		_enemyCars.callAll("drawSkids", []);
		
		_enemyCars.draw();
        
        _pickUpList.draw();
        
		_player.draw();
		_shotList.draw();
        
		_vignette.draw();
		_overlay.draw();
		
        _infoText2.draw();
        _infoText.draw();
        
		_scoreText2.draw();
		_scoreText.draw();

        
		_player.drawHud();
		super.draw();
	}
	
	private function overlapPlayerTiles ( p: Car, t: Tile) : Void 
	{
		//trace ("overlap");
		p.TakeDamage(GameProperties.Damage_CollisionDamage * t.blockDamageFactor);
		p.AddSkids();
		if (t._destroyable)
		{
			t.alive = false;
		}
	}
    
    private function overlapPlayerPickups ( p : Player, pu : PickUp) : Void
    {
        if (pu.alive&& !pu.ending)
        {
            pu.PickMeUp();
			ShowInfoText(pu.x, pu.y, pu.text);
            if (pu._type == PickUpType.Points)
            {
                _score += GameProperties.Score_PickUpIncome * Math.pow(GameProperties.Tile_MoveSpeedFactor, 0.25);
				_sfxScore.play();
            }
            else
            {
				if (pu._type == PickUpType.MachineGun)
					_sfxAmmunition.play();
				if (pu._type == PickUpType.Repair)
					_sfxRepair.play();
					
				_score += 0.125*GameProperties.Score_PickUpIncome * Math.pow(GameProperties.Tile_MoveSpeedFactor, 0.25);
                p.pickUp(pu);
            }
        }
    }
	
	private function PushNewLine(): Bool 
	{
		// find topTile
		var topPos : Float = 10;
		var ret : Bool = true;
		_tiles.forEach(function(t:Tile):Void 
		{
			if (t.y < topPos)
			{
				topPos = t.y;
				if ( topPos < - 2 * GameProperties.Tile_Height)	// no need to search further (performance gain?)
				{	
					ret = false;
					return;
				}
			}
		});
		
		// create new line
		if (topPos >= - 2 * GameProperties.Tile_Height)
		{
            RoadCreator.Create(_center, _width, topPos, _tiles, _tilesBlocked );
        }
        
		return ret;
	}
	
	private function changeRoadParameters () : Void 
	{
		var newCenter :Int = _center;
		var newWidth :Int = _width;
		
		var changeCenter : Bool = FlxRandom.chanceRoll(50);
		
		if (changeCenter)
		{
			newCenter += FlxRandom.intRanged( -1, 1, [0]);
			if ( newCenter - _width/2 < 1)
			{
				newCenter = Std.int( 1 + _width / 2);
			}
			else if (newCenter + _width/2 > 12)
			{
				newCenter = Std.int(12 - _width/2);
			}
		}
		// then change center // don't change both, this looks ugly very easily
		else 
		{
			newWidth += FlxRandom.intRanged( -1, 1, [0]);
			if ( newWidth < _minWidth)
			{
				newWidth = _minWidth;
			}
			else if (newWidth > _maxWidth )
			{
				newWidth =_maxWidth;
			}
		}
		
		_center = newCenter;
		_width = newWidth;
		var t : FlxTimer = new FlxTimer(GameProperties.Tile_NewRoadStuffTimer, function (t:FlxTimer) : Void { changeRoadParameters(); }, 1);
	}
	
	private function speedUp() : Void 
	{
		trace(GameProperties.Tile_CalcMoveSpeed() );
		GameProperties.Tile_MoveSpeedFactor += GameProperties.Tile_MoveSpeedFactorIncrease;
		if (GameProperties.Tile_MoveSpeedFactor > 1.75)
		{
			//GameProperties.Tile_MoveSpeedFactor += GameProperties.Tile_MoveSpeedFactorIncrease;
			_maxWidth = 4;
			_minWidth = 3;
			GameProperties.Tile_RockSpawnChance = 4;
		}
		
		if (GameProperties.Tile_MoveSpeedFactor > 2.25)
		{
			_maxWidth = 3;
			_minWidth = 2;
			GameProperties.Tile_MoveSpeedFactor += GameProperties.Tile_MoveSpeedFactorIncrease * 2; 
			GameProperties.Tile_RockSpawnChance = 6;
		}
		if (GameProperties.Tile_MoveSpeedFactor > 3.5)
		{
			_maxWidth = 2;
			_minWidth = 2;
			//GameProperties.Tile_MoveSpeedFactor += GameProperties.Tile_MoveSpeedFactorIncrease * 2; 
			GameProperties.Tile_RockSpawnChance = 9;
		}
	}
	
	
	private function endGame() : Void 
	{
		if (!_ending)
		{
			FlxGameJolt.addScore("Main", _score);
			_ending = true;
			FlxG.camera.fade(FlxColor.BLACK, 1.0, false, function () : Void { FlxG.switchState(new MenuState()); } );
			//FlxGameJolt.fetchScore(
		}	
	}
	
	function initializeRoad():Void 
	{
		_tiles = new FlxTypedGroup<Tile>();
		_tilesBlocked = new FlxTypedGroup<Tile>();
		_center = 6;
		_width = 3;
		_maxWidth = 5;
		_minWidth = 4;
		
		for (j in 0...10)
		{
			var roadLeftBorder : Int = _center - _width +1;
			var roadRightBorder : Int = _center + _width;
			for (i in -1...roadLeftBorder)
			{
				var t :Tile  = new Tile(TileType.Grass, new FlxPoint(i * GameProperties.Tile_Height, j * GameProperties.Tile_Height));
				_tilesBlocked.add(t);
			}
			for (i in roadLeftBorder...roadRightBorder)
			{
				var t : Tile = new Tile(TileType.Road, new FlxPoint(i * GameProperties.Tile_Height, j * GameProperties.Tile_Height));
				_tiles.add(t);
			}
			for (i in roadRightBorder...14)
			{
				var t :Tile = new Tile(TileType.Grass, new FlxPoint(i * GameProperties.Tile_Height, j * GameProperties.Tile_Height));
				_tilesBlocked.add(t);
			}
			while (FlxRandom.chanceRoll(50))
			{
				var px : Float = Random.floatRangeExclude( -32, FlxG.width + 32, (roadLeftBorder -1) * 64, roadRightBorder * 64);
				var t : Tile = new Tile(TileType.Tree, new FlxPoint( px ,  -GameProperties.Tile_Height));
				_tilesBlocked.add(t);
			}
		}
	}
	
	function initScore():Void 
	{
		_scoreText2 = new FlxText(0, 0, 300);
		_scoreText2.text = "0";
		_scoreText2.setFormat("assets/data/AeroviasBrasilNF.ttf", 45, FlxColor.CRIMSON,"right");
		//_scoreText2.screenCenter();
		//_scoreText2.x = FlxG.width - this._width - 10;
		_scoreText2.y = 10;
		_scoreText2.x = 450;
		_scoreText2.offset.set( -4, 4);
		FlxTween.color(_scoreText2, 2.5, FlxColor.CRIMSON, FlxColor.AQUAMARINE, 0.5, 0.5, {type:FlxTween.PINGPONG} );
		
		_scoreText = new FlxText(0, 0, 300);
		_scoreText.text = "0";
		_scoreText.setFormat("assets/data/AeroviasBrasilNF.ttf", 45, FlxColor.CRIMSON,"right");
		_scoreText.x = 450;
		_scoreText.y = 10;
		FlxTween.color(_scoreText, 25, FlxColor.AQUAMARINE, FlxColor.CRIMSON, 1.0, 1.0, { type:FlxTween.PINGPONG } );
        
        _infoText2 = new FlxText(0, 0, 500);
        _infoText2.text = "0";
		_infoText2.setFormat("assets/data/AeroviasBrasilNF.ttf", 30, FlxColor.CRIMSON,"left");
        _infoText2.setPosition(2000, 2000);
		_infoText2.offset.set( -4, 4);
		FlxTween.color(_infoText2, 1.25, FlxColor.CRIMSON, FlxColor.AQUAMARINE, 0.5, 0.5, { type:FlxTween.PINGPONG } );
        
        _infoText = new FlxText(0, 0, 500);
		_infoText.text = "0";
		_infoText.setFormat("assets/data/AeroviasBrasilNF.ttf", 30, FlxColor.CRIMSON,"left");
        _infoText.setPosition(2000, 2000);
        FlxTween.color(_infoText, 1.25, FlxColor.AQUAMARINE, FlxColor.CRIMSON, 1.0, 1.0, { type:FlxTween.PINGPONG } );
        
	}
	
	function SpawnEnemy():Void 
	{
		var s : Bool = FlxRandom.chanceRoll(50);
		var e: Enemy = new Enemy(s);
		e.x = _center * GameProperties.Tile_Height;
		e.y = (s ? - GameProperties.Tile_Height : FlxG.height + GameProperties.Tile_Height);
        _enemySpawnTimer = GameProperties.Enemy_SpawnTimerMax;
		_enemyCars.add(e);
	}
    
    function ShowInfoText(posX : Float, posY: Float, t: String, time:Float = 2.5) : Void 
    {
        _infoText.text = t;
        _infoText2.text = t;
        _infoText.setPosition(posX, posY);
        _infoText2.setPosition(posX, posY);

        var t: FlxTimer = new FlxTimer(time, function (t:FlxTimer) : Void 
        { 
            _infoText.setPosition(2000, 2000);
            _infoText2.setPosition(2000, 2000);
        }, 1);
    }
    
    function SpawnPickUp(introPU:Bool = false):Void 
    {
		var t: PickUpType = (introPU ? PickUpType.Points : PickUp.getRandomPickupType());
        var pu : PickUp = new PickUp(t);
        pu.x = _center * GameProperties.Tile_Height;
        pu.y = - GameProperties.Tile_Height;
        
        _pickUpList.add(pu);
		_pickUpSpawnTimer = FlxRandom.floatRanged(3, 6);
    }
	
	public function SpawnShot(s:Shot)
	{
		_shotList.add(s);
	}
	
}
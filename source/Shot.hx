package ;

import flixel.FlxSprite;
import flixel.util.FlxRandom;

/**
 * ...
 * @author 
 */
class Shot extends FlxSprite
{

	public function new(X:Float, Y:Float) 
	{
		super(X, Y);
		this.loadGraphic(AssetPaths.shot__png, false, 5, 5);
		velocity.set(FlxRandom.floatRanged( -4, 4), -600);
	}
	
}